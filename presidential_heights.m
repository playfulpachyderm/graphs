a = [188, 170, 189, 163, 183, 171, 185, 168, 173, 183, 173, 173, 175, 178, 183, 193, 178, 173, 174, 183, 188, 180, 168, 170, 178, 182, 180, 183, 178, 182, 188, 175, 179, 185, 192, 180, 183, 177, 185, 188, 188, 182, 185, 191];

x = linspace(1, 44, 44);

function [ft, in] = feet(cm)
	inches = cm / 2.54;
	ft = floor(inches / 12);
	in = mod(inches, 12);
end

plot(a);
figure(1,'position',get(0,'screensize'));

ylim([0 200]);
ylabel('Height (cm)');
xlabel('President number');

hold on

mckinley = 24;
scatter(mckinley, a(mckinley));
text(mckinley + 0.5, a(mckinley) - 2, 'William McKinley (1892)');

zuck_height = (5*12 + 8) * 2.54;

plot(x, ones(1, 44).*zuck_height);
hold off

pause
