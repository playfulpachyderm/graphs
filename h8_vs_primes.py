import oct2py

HS = [i in (1, 5, 6, 11, 12, 13, 14, 18, 23, 28, 33, 38, 43, 83, 88, 100) for i in range(1, 101)]

P = [True] * 101
P[1] = False
for i in range(2, 100):
	for j in range(i*2, 100, i):
		P[j] = False
del P[0]

octave = oct2py.Oct2Py()

h_vals = [0]
p_vals = [0]

for i in range(100):
	h_vals.append(h_vals[-1] + int(HS[i]))
	p_vals.append(p_vals[-1] + int(P[i]))

x_vals = octave.linspace(0, 100, 101)

octave.hold("on")
octave.plot(x_vals, h_vals, color="green")
octave.plot(x_vals, p_vals, color="red")

octave.legend("Hate symbols (according to the ADL)", "Prime numbers")
octave.title("Density of prime numbers and hate symbols in the low integers")
octave.xlabel("Integers")
octave.ylabel("Accumulated entities")
