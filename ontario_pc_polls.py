import sys
import codecs
from time import time, strptime

import numpy
from oct2py import Oct2Py
from bs4 import BeautifulSoup
import requests

sys.path += ["C:/Users/User/vagrant/projects/cryptomath"]
from tableparse import Table, download_table  # pylint: disable=import-error
from smooth import smooth


class Poll():
    def __init__(self, date, con, lib, ndp):
        self.date = date
        self.con = con
        self.lib = lib
        self.ndp = ndp
    def margin(self):
        return self.con - max(self.lib, self.ndp)
    def __str__(self):
        return "{self.date}, {self.con}, {self.lib}, {self.ndp}".format(self=self)


def date_number(d):
    return (2018 - d.tm_year) * 365 + 158 - d.tm_yday

def fetch_data():
    url = "https://en.wikipedia.org/wiki/Ontario_general_election,_2018"
    selector = ".wikitable"
    table = Table(None, soup=download_table(url, selector, -1))
    table2 = Table(None, soup=download_table(url, selector, -2))
    table.print_headers()
    table2.print_headers()
    print(table.headers)
    # with codecs.open("out.txt", "w", "utf-8") as f:
    #     f.write(requests.get(url).text)

    # first 2 rows are empty
    del table.data[0]
    del table.data[0]
    del table2.data[0]

    con = table2.get_column(4) + table.get_column(4)
    lib = table2.get_column(3) + table.get_column(3)
    ndp = table2.get_column(5) + table.get_column(5)

    polls = []

    for i, date in enumerate(table2.get_column(1) + table.get_column(1)):
        try:
            datestring = date.split("0000")[-1]
            d = strptime(datestring, "%B %d %Y")
        except (AttributeError, ValueError):
            # no column 1; it's a single-column row, like a debate or something
            continue

        poll = Poll(date_number(d), con[i], lib[i], ndp[i])
        polls.append(poll)

    return polls

def smoothfunc(l):
    window = 2
    return smooth(l, window_L=window, window_R=window)


polls = fetch_data()
dates = [poll.date for poll in polls]



octave = Oct2Py()
octave.hold("on")

# Parties
con_line = octave.plot(dates, smoothfunc([poll.con for poll in polls]), color="blue")
lib_line = octave.plot(dates, smoothfunc([poll.lib for poll in polls]), color="red")
ndp_line = octave.plot(dates, smoothfunc([poll.ndp for poll in polls]), "color", (1, 0.6, 0.2))

# PC margin
margin_line = octave.plot(dates, smoothfunc([poll.margin() for poll in polls]), color="blue", linewidth=3)

octave.title("Polling between 2014 and 2018 Ontario elections")
octave.set(octave.gca(), "xdir", "reverse")
octave.xlim((0,1500))
octave.xlabel("Days until election (June 7, 2018)")
octave.ylabel("Polling at (%)")

# Rob Ford is nominated (vertical line)
ford = 158 - 69
ford_line = octave.plot([ford, ford], [-20, 60], "color", [0.6, 0.6, 0.6], linewidth=1, linestyle="--")
ford_arrow1 = octave.line([230, 110], [55, 55])
ford_arrow2 = octave.line([130, 110], [56, 55])
ford_arrow3 = octave.line([130, 110], [54, 55])
ford_label = octave.text(350, 55, "Doug Ford nominated")

# Debates (CityTV Toronto, Parry Sound)
debates = (158 - 127, 158 - 131)
debate_lines = []
for d in debates:
    debate_lines.append(
        octave.plot([d, d], [-20, 60], "color", [0.8, 0.8, 0.8], linestyle="--")
    )

# scatter(mckinley, a(mckinley));
# text(mckinley + 0.5, a(mckinley) - 2, 'William McKinley (1892)');

# ford_label = octave.annotation("textarrow", [ford-1, ford - 10], [50, 50])

# Previous provincial election (vertical line)
wynne_line = octave.plot([1456, 1456], [-20, 60], "color", [0.6, 0.6, 0.6], linewidth=1)

# X-axis (horizontal line)
octave.plot([0, 1500], [0, 0], color="black")

# Election date (vertical line)
deadline = octave.plot([0, 0], [-20, 60], color="black")

# Line of best fit
lobf = octave.polyfit(dates, [poll.margin() for poll in polls], 1)
x_vals = octave.linspace(0, 1500, 1500)
octave.plot(x_vals, octave.polyval(lobf, x_vals))

labels = {
    con_line: "PC Party",
    lib_line: "Liberal Party",
    ndp_line: "NDP",
    margin_line: "PC margin (advantage)",
}

legend = octave.legend(list(labels.keys()), *labels.values(), "location", "north")
